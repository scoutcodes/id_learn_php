<?php
session_start();
require '../functions.php';
if (!empty($_POST)) {
// MySQL Connect.
  $db = connect();
  $user = user_get_current_user();
  $title = $_POST['title'];
  $content = $_POST['content'];
  $lang = current_language();
  $args = array(
    "INSERT INTO content(uid, created, title, content) VALUES ('%d', '%d', '%s', '%s')",
    $user['uid'],
    time(),
    mysql_escape_string(serialize(array($lang => $title))),
    mysql_escape_string(serialize(array($lang => $content))),
  );
  $query = call_user_func_array('sprintf', $args);

  $insert = mysql_query($query, $db);
  $cid = mysql_insert_id();
  $_SESSION['message'] = sprintf('Content "%s" has been created.', $title);
  mysql_close($db);
  header("Location: ../content.php?cid=" . $cid);
}