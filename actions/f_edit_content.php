<?php
session_start();
require '../functions.php';
if (!empty($_POST)) {
  $db = connect();
  $lang = current_language();
  $title = serialize(array($lang => $_POST['title']));
  $content = $_POST['content'];
  $cid = $_POST['cid'];

  $old_title = content_get_field_value($cid, 'title');
  $old_body = content_get_field_value($cid, 'content');
  if ($old_title) {
    $query = sprintf("UPDATE content SET title='%s' WHERE cid='%d'", $title, $cid);
  }
  else {
    $query = sprintf("INSERT INTO content(title) VALUES ('%s') WHERE cid='%d'", $title, $cid);
  }
  mysql_query($query, $db);

  if ($old_body) {
    $query = sprintf("UPDATE content SET content='%s' WHERE cid='%d'",
      serialize(array($lang => $content)),
      $cid);
  }
  else {
    $query = sprintf("INSERT INTO content(content) VALUES ('%s') WHERE cid='%d'", $content, $cid);
  }
  mysql_query($query, $db);


//  $query = sprintf("UPDATE content SET created='%d', title='%s', content='%s' WHERE cid='%d'",
//  time(),
//  mysql_escape_string($title),
//  mysql_escape_string(serialize(array($lang => $content))),
//  $cid);
//
//  $args = array(
//    "INSERT INTO content(created, title, content_ua) VALUES ('%d', '%s', '%s') WHERE cid='%d'",
//    time(),
//    mysql_escape_string($title),
//    mysql_escape_string(serialize(array($lang => $content))),
//    $cid,
//  );
//  $query = call_user_func_array('sprintf', $args);
//  $insert = mysql_query($query, $db);
  $_SESSION['message'] = sprintf('Content "%s" has been edited.', $_POST['title']);
  mysql_close($db);
  header("Location: ../content.php?cid=" . $cid);
}