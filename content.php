<?php $cid = $_GET['cid']; ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Home</title>
  <script type="text/javascript" src="bootstrap/js/bootstrap.js"></script>
  <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="bootstrap/css/global.css">
</head>
<body>
<?php require_once 'layout/header.php'; ?>
  <div class="well bs-component">
    <div class="my-blog">
      <h2><a href="content.php?cid=<?php print $cid; ?>"><?php print content_get_field_value($cid, 'title'); ?></a></h2>
      <div><span class="text-muted"></span><p class="text-info"><?php print_r(user_get_current_user()['name']) ?></p></div>
      <p class="format-content"><?php print content_get_field_value($cid, 'content'); ?></p>
    </div>
    <br>
    <div class="btn-group btn-group-justified">
      <a href="edit_content.php?cid=<?php print $cid; ?>" class="btn btn-default">Edit</a>
      <a href="delete_current_content.php" class="btn btn-default">Delete</a>
    </div>
  </div>
</body>
</html>
