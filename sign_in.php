<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Home</title>
  <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="bootstrap/css/global.css">
  <script type="text/javascript" src="bootstrap/js/bootstrap.js"></script>
</head>
<body>
<?php require 'layout/header.php'; ?>
<form class="form-horizontal" action="actions/f_sign_in.php" method="POST">
  <fieldset>
    <h1 align="center"><?php print t('Please, do!') ?></h1>
    <br>
    <div class="well bs-component">
      <div class="form-group">
        <label for="inputEmail" class="col-lg-2 control-label"><?php print t('Username'); ?></label>
        <div class="col-lg-10">
          <input type="text" class="form-control" id="inputEmail" name="username" placeholder="<?php print t('Username'); ?>">
        </div>
      </div>
      <div class="form-group">
        <label for="inputPassword" class="col-lg-2 control-label"><?php print t('Password'); ?></label>
        <div class="col-lg-10">
          <input type="password" name="password" class="form-control" id="inputPassword" placeholder="<?php print t('Password'); ?>">
        </div>
      </div>
      <div class="form-group">
        <div class="col-lg-10 col-lg-offset-2">
          <button type="submit" class="btn btn-primary"><?php print t('Go!'); ?></button>
        </div>
      </div>
    </div>
  </fieldset>
</form>
</body>
</html>