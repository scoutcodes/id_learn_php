<?php
$body = $value['content'];
$body = unserialize($body);
$lg = current_language();
if (!empty($body[$lg])) {
  $body = $body[$lg];
}
else {
  $body = reset($body);
}