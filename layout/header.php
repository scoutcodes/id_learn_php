<?php session_start(); ?>
<?php require_once 'functions.php'; ?>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php">IDevels</a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="index.php"><?php print t('Home'); ?><span class="sr-only">(current)</span></a></li>
        <?php if (!empty($_SESSION['username'])): ?>
          <li>
            <a href="add_content.php"><?php print t('Add content'); ?></a>
          </li>
        <?php else: ?>
          <li>
            <a href="sign_in.php"><?php print t('Add content'); ?></a>
          </li>
        <?php endif; ?>
      </ul>
      <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="<?php print t('Search'); ?>">
        </div>
        <button type="submit" class="btn btn-default"><?php print t('Go!');; ?></button>
      </form>
      <ul class="nav navbar-nav navbar-right">
        <?php if (!empty($_SESSION['username'])): ?>
          <li class="active">
            <a href="user.php"><?php print $_SESSION['username'] ?></a>
          </li>
          <li>
            <a href="logout.php"><?php print t('Log Out'); ?></a>
          </li>
        <?php else: ?>
          <li>
            <a href="sign_in.php"><?php print t('Log In'); ?></a>
          </li>
          <li>
            <a href="register.php"><?php print t('Sign Up'); ?></a>
          </li>
        <?php endif; ?>
      </ul>
    </div>
  </div>
</nav>
<div class="btn-group" role="group" aria-label="...">
  <a href="lang/ua.php"><button type="button" class="btn btn-default"> Ua</button></a>
  <a href="lang/en.php"><button type="button" class="btn btn-default"> En</button></a>
</div>
<?php print show_message(); ?>
