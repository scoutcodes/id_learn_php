<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Register</title>
  <script type="text/javascript" src="bootstrap/js/bootstrap.js"></script>
  <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="bootstrap/css/global.css">
</head>
<body>
<?php require 'layout/header.php'; ?>
<form class="form-horizontal" action="actions/f_register.php" method="POST">
  <fieldset>
    <h1 align="center">Please, register now!</h1>
    <br>
    <div class="well bs-component">
      <div class="form-group">
        <label for="inputEmail" class="col-lg-2 control-label">Username</label>
        <div class="col-lg-10">
          <input name="username" type="text" required class="form-control" id="inputEmail" placeholder="Username">
        </div>
      </div>
      <div class="form-group">
        <label for="inputPassword" class="col-lg-2 control-label">Password</label>
        <div class="col-lg-10">
          <input name="password" type="password" required class="form-control" id="inputPassword" placeholder="Password">
        </div>
      </div>
      <div class="form-group">
        <div class="col-lg-10 col-lg-offset-2">
          <button type="submit" class="btn btn-primary">Register</button>
        </div>
      </div>
    </div>
  </fieldset>
</form>
</body>
</html>