<?php

//GET MYSQL CONNECT.
function connect() {
  $db = mysql_connect('localhost', 'root', '111');
  mysql_select_db('study', $db);
  return $db;
}

//Get status message.
function show_message() {
  $out = '';
  if (!empty($_SESSION['message'])) {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
    $type = 'success';
    if (is_array($message)) {
      $type = key($message);
    }
    $out .= '<div class="alert alert-dismissible alert-' . $type . '">';
    $out .= '<button type="button" class="close" data-dismiss="alert">x</button><strong>';
    $out .= ucfirst($type) . '!!! ';
    $out .= '</strong>';
    $out .= is_array($message) ? reset($message) : $message;
    $out .= '</div>';
  }
  return $out;
}

//Get hash password.
function get_hash_pass($pass) {
  $pass = md5($pass);
  $pass = $pass.'L0l11';
  return $pass;
}

//Get user name.
function user_get_name() {
  print $_SESSION['username'];
}

//Get user status.
function user_is_autorized() {
  if ($_SESSION['username']) {
    print 'User is authorized';
  } else {
    print "User isn't authorized";
  }
}

//Get info from data base via user id.
function user_load($uid) {
  $db = connect();
  $select = sprintf("SELECT * FROM users WHERE uid = '%s'", $uid);
  $query = mysql_query($select, $db);
  $results = mysql_fetch_assoc($query);
  return $results;
}

//Get current user from data base.
function user_get_current_user() {
  $db = connect();
  $username = $_SESSION['username'];
  $select = sprintf("SELECT * FROM users WHERE name = '%s'", $username);
  $query = mysql_query($select, $db);
  $results = mysql_fetch_assoc($query);
  return $results;
}

//Delete current user from data base.
function delete_current_user() {
  $db = connect();
  $username = $_SESSION['username'];
  $select = sprintf("DELETE FROM users WHERE name = '%s'", $username);
  $query = mysql_query($select, $db);
  $results = mysql_fetch_assoc($query);
  return $results;
}

//Get info from data base via content id.
function content_load($cid) {
  $db = connect();
  $select = sprintf("SELECT * FROM content WHERE cid = '%d'", $cid);
  $query = mysql_query($select, $db);
  $results = mysql_fetch_assoc($query);
  return $results;
}

//Return content from data base.
function return_content() {
  $db = connect();
  $result = mysql_query('SELECT * FROM content', $db);
  $content = array();
  if ($row = mysql_fetch_assoc($result)) {
    $content[] = $row;
  }
  return $content;
}

//Update current user to data base.
function update_current_user() {
}

//Delete all user content from data base.
function delete_all_user_content() {
  $db = connect();
  $a = user_get_current_user()['uid'];
  $del = sprintf("DELETE FROM content WHERE uid = '%d'", $a);
  $del_query = mysql_query($del, $db);
  return $del_query;
}

//Redirect user to input path.
function user_redirect($path, $params = array()) {

}

//Return all content from data base.
function content_load_multiple() {
  $db = connect();
  $content = array();
  $result = mysql_query("SELECT * FROM content ORDER BY created DESC", $db);
  while ($row = mysql_fetch_assoc($result)) {
    $content[] = $row;
  }
  return $content;
}

//Load content for current user.
function content_load_current_user() {
  $db = connect();
  $content = array();
  $uid = user_get_current_user()['uid'];
  $select = sprintf("SELECT * FROM content WHERE uid = '%d' ORDER BY cid DESC", $uid);
  $result = mysql_query($select, $db);
  while ($row = mysql_fetch_assoc($result)) {
    $content[] = $row;
  }
  return $content;
}

//Delete current content from data base.
function delete_current_content() {
  $db = connect();
  $cid = return_content();
  foreach ($cid as $key => $value) {
    $del = sprintf("DELETE FROM content WHERE cid='%d'", $value['cid']);
    $del_query = mysql_query($del, $db);
    return $del_query;
  }
}

//
function return_domain() {
  $way = $_SERVER['SCRIPT_NAME'];
  return $way;
}

//Return changed language.
function t($source) {
  if(!empty($_SESSION['language']) && $_SESSION['language'] == 'en') {
    return $source;
  }
  $db = connect();
  mysql_set_charset('utf8', $db);
  $select = sprintf("SELECT ua FROM lang1 WHERE en = '%s'", $source);
  $query = mysql_query($select, $db);
  $result = mysql_fetch_row($query);
  if (empty($result)) {
    return $source;
  }
  return reset($result);
}

//Return name of current language.
function current_language() {
  if(!empty($_SESSION['language'])) {
    return $_SESSION['language'];
  }
  return 'en';
}

//
function content_get_field_value($cid, $field) {
  $content = content_load($cid);
  if (isset($content[$field])) {
    $value = $content[$field];
    $value = unserialize($value);
    $language = current_language();

    return isset($value[$language]) ? $value[$language] : '';
  }
  return '';
}






//Two ways for crypt our passwords.
/*<?php
$passc = password_hash("olegkruchay", PASSWORD_DEFAULT)."\n";
$pass = crypt('scout', $passc);

$pass = '1';

$pass = md5($pass);
$pass = $pass.'L0l11';
echo $pass;
?>
*/
