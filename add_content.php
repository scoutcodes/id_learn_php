<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Home</title>
  <script type="text/javascript" src="bootstrap/js/bootstrap.js"></script>
  <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
</head>
<body>
<?php require 'layout/header.php'; ?>
<h1 align="center">Content</h1><br>
  <div class="well bs-component">
    <div class="form-horizontal">
      <fieldset>
        <form id="content" action="actions/f_add_content.php" method="POST">
          <div class="form-group">
            <label for="inputEmail" class="col-lg-2 control-label">Title</label>
            <div class="col-lg-10">
              <input type="text" class="form-control" id="inputTitle" name="title" placeholder="Title">
            </div>
            <label for="textArea" class="col-lg-2 control-label">Input your own text!</label>
            <div class="col-lg-10">
              <textarea name="content" form="content" class="form-control" rows="3" id="textArea" style="margin: 0px -16px 0px 0px; height: 87px; width: 100%; max-width: 1680px;"></textarea>
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
              <button type="reset" class="btn btn-default">Cancel</button>
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </div>
          <a href="content.php">You can see your last posts.</a>
        </form>
      </fieldset>
    </div>
  </div>
</body>
</html>