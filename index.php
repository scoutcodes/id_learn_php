<?php
require 'functions.php';
$content = content_load_multiple();
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>My site</title>
  <script type="text/javascript" src="bootstrap/js/bootstrap.js"></script>
  <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="bootstrap/css/global.css">
</head>
<body>
<?php require 'layout/header.php'; ?>
<br>
<?php print current_language(); ?>
<h1 align="center"><strong><?php print t('Welcome'); ?>, <?php !empty($_SESSION['username']) ? print $_SESSION['username'] : print t('User'); ?>!</strong></h1>
<br>
<div class="well bs-component">
  <?php foreach($content as $key => $value) : ?>
    <div class="my-blog">
      <h2><?php if (!empty($_SESSION['username'])) : ?> <a href="content.php?cid=<?php print $value['cid']; ?>"><?php endif; ?><?php print content_get_field_value($value['cid'], 'title'); ?></a></h2>
      <div><span class="text-muted"><?php print date('D, j M Y G:i:s' ,$value['created']); ?></span><p class="text-info"><?php print_r(user_load($value['uid'])['name']) ?></p></div>
      <p class="format-content">
        <?php print content_get_field_value($value['cid'], 'content'); ?></p>
      <br>
    </div>
  <?php endforeach; ?>
</div>
</body>
</html>

