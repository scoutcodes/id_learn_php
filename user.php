<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Home</title>
  <script type="text/javascript" src="bootstrap/js/bootstrap.js"></script>
  <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
</head>
<body>
<?php require 'layout/header.php'; ?>
<h1 align="center">User page</h1>
  <hr>
  <br>
  <h1 align="center"><strong>Dear, <?php !empty($_SESSION['username']) ? print $_SESSION['username'] : print 'User' ?>!</strong></h1>
  <h2 align="center">You are on your own page.</h2>
  <h3 align="center">You can <a href="#">edit</a> or <a href="delete_user.php">delete</a> your personal infornation.</h3>
  <div class="well bs-component">
    <form action="update_user.php" method="POST">
      <label for="inputEmail" class="col-lg-2 control-label">Username:</label>
      <div class="col-lg-10">
        <input type="text" class="form-control" id="inputTitle" name="username" placeholder="<?php !empty($_SESSION['username']) ? print $_SESSION['username'] : print 'User' ?>">
      </div>
      <label for="inputEmail" class="col-lg-2 control-label">Password</label>
      <div class="col-lg-10">
        <input type="text" class="form-control" id="inputTitle" name="password" placeholder="******">
      </div>
      <br>
      <div class="col-lg-10 col-lg-offset-2">
        <button type="submit" class="btn btn-primary" title="Edit">Save</button>
      </div>
    </form>
    <h5 align="center"><p>You can <a href="delete_content.php">delete</a> all your content.</p></h5>
    <p align="center"><a href="my_content.php">See my content</a></p>
  </div>
</body>
</html>