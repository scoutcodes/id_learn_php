<?php
require 'functions.php';
$content = content_load_current_user();
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>My content</title>
  <script type="text/javascript" src="bootstrap/js/bootstrap.js"></script>
  <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="bootstrap/css/global.css">
</head>
<body>
<?php require 'layout/header.php'; ?>
<h1 align="center"><strong>Hey, <?php !empty($_SESSION['username']) ? print $_SESSION['username'] : print 'User' ?>!</strong></h1>
<br>
<div class="well bs-component">
  <?php  ?>
</div>
</body>
</html>

